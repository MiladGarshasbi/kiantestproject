package org.themoviedb.testapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.themoviedb.testapp.model.MovieDetailsResult
import org.themoviedb.testapp.network.Repository.MovieListRepository
import org.themoviedb.testapp.network.State
import java.util.concurrent.TimeUnit

class MovieDetailsViewModel(
    private val movieId: Int,
    private val movieListRepository: MovieListRepository
) : ViewModel() {

    private var loadingVisibility = MutableLiveData<Boolean>(true)
    private var state: MutableLiveData<State> = MutableLiveData()
    private var result = MutableLiveData<MovieDetailsResult>()
    private lateinit var subscription: Disposable

    init {
        getData()
    }


    fun getLoadingVisibility(): MutableLiveData<Boolean> {
        return loadingVisibility
    }

    fun getState():MutableLiveData<State>{
        return state
    }

    private fun getData() {

        subscription = movieListRepository.getMovieDetails(movieId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .timeout(10,TimeUnit.SECONDS)
            .subscribe(
                { result ->
                    onRetrievMovieListSucces(result)
                },
                {
                    onRetriveMovieListError()
                })

    }

    private fun onRetriveMovieListError() {
        loadingVisibility.postValue(false)
        state.postValue(State.ERROR)
    }

    private fun onRetrievMovieListSucces(results: MovieDetailsResult) {
        loadingVisibility.postValue(false)
        state.postValue(State.DONE)
        result.value = results
    }

    fun getResult(): LiveData<MovieDetailsResult> {
        return result
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}
