package org.themoviedb.testapp.viewmodel

import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.reactivex.disposables.CompositeDisposable
import org.themoviedb.testapp.model.database.Movie
import org.themoviedb.testapp.network.Repository.MovieListRepository
import org.themoviedb.testapp.network.Repository.MovieListRepositoryFactory
import org.themoviedb.testapp.network.State

class MoviesListViewModel(private val movieListRepository: MovieListRepository) : ViewModel() {

    private var loadingVisibility = MutableLiveData<Boolean>(false)
    private var moviePagedList: LiveData<PagedList<Movie>>
    private val compositeDisposable = CompositeDisposable()
    private var movielistrepositoryFactory: MovieListRepositoryFactory

    init {
        movielistrepositoryFactory = MovieListRepositoryFactory(
            movieListRepository.getMovieApi(),
            movieListRepository.getMovieDao(), compositeDisposable
        )
        val config = getPageListConfig()
        moviePagedList =
            LivePagedListBuilder<Int, Movie>(movielistrepositoryFactory, config).build()
    }

    private fun createPageList() {
        val config = getPageListConfig()
        moviePagedList =
            LivePagedListBuilder<Int, Movie>(movielistrepositoryFactory, config).build()
    }

    private fun getPageListConfig(): PagedList.Config {
        val config = PagedList.Config.Builder()
            .setPageSize(20)
            .setInitialLoadSizeHint(40)
            .setEnablePlaceholders(false)
            .build()
        return config
    }

    fun getMoviePagedList(): LiveData<PagedList<Movie>> {
        return moviePagedList
    }

    fun getState(): LiveData<State> = Transformations
        .switchMap<MovieListRepository, State>(
            movielistrepositoryFactory.movieListRepositoryLiveData,
            MovieListRepository::getState
        )


    fun listIsEmpty(): Boolean {
        return moviePagedList.value?.isEmpty() ?: true
    }


    fun getLoadingVisibility(): Boolean {
        return loadingVisibility.value!!
    }

    fun onFilterClick(lifecycleOwner: LifecycleOwner,filter :String) {
        moviePagedList.removeObservers(lifecycleOwner)
        movielistrepositoryFactory = MovieListRepositoryFactory(
            movieListRepository.getMovieApi(),
            movieListRepository.getMovieDao(), compositeDisposable,filterString = filter
        )
      createPageList()
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }


}
