package org.themoviedb.testapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.themoviedb.testapp.network.Repository.MovieListRepository


class MoviesDetailsViewModelFactory(private var extra: Int,private var movieListRepository: MovieListRepository) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
           return MovieDetailsViewModel(extra,movieListRepository) as T

    }
}