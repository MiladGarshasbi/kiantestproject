package org.themoviedb.testapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.themoviedb.testapp.model.database.Movie
import org.themoviedb.testapp.utils.IMAGE_BASE_URL

class MovieViewModel : ViewModel() {


    private val title = MutableLiveData<String>()
    private val rating = MutableLiveData<String>()
    private val imageUrl = MutableLiveData<String>()
    private val date = MutableLiveData<String>()
    private val description = MutableLiveData<String>()


    fun bind(movie: Movie) {
        title.value = movie.title
        rating.value = movie.vote_count.toString()
        imageUrl.value = IMAGE_BASE_URL+movie.poster_path
        date.value = movie.release_date
        description.value = movie.overview

    }

    fun getTitle(): MutableLiveData<String> {
        return title
    }

    fun getRating(): MutableLiveData<String> {
        return rating
    }

    fun getImageUrl(): MutableLiveData<String> {
        return imageUrl
    }

    fun getDate(): MutableLiveData<String> {
        return date
    }

    fun getDescription(): MutableLiveData<String> {
        return description
    }


}

