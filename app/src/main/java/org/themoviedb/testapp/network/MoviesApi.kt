package org.themoviedb.testapp.network

import androidx.lifecycle.LiveData
import io.reactivex.Observable
import io.reactivex.Single
import org.themoviedb.testapp.model.MovieDetailsResult
import org.themoviedb.testapp.model.MoviesListResult
import org.themoviedb.testapp.utils.API_KEY
import org.themoviedb.testapp.utils.MOVIE_LIST_URL
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface MoviesApi {

    @FormUrlEncoded
    @POST(MOVIE_LIST_URL)
    fun getMovies(@Field("page") page: String): Single<Response<MoviesListResult>>

    @FormUrlEncoded
    @POST(MOVIE_LIST_URL)
    fun getMoviesSync(@Field("page") page: String): Call<MoviesListResult>


    @GET("{movie_id}?api_key=${API_KEY}")
    fun getMovieDetails(@Path("movie_id") movie_id: String): Single<MovieDetailsResult>

}