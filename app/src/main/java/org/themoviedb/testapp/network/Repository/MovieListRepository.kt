package org.themoviedb.testapp.network.Repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.themoviedb.testapp.model.database.Movie
import org.themoviedb.testapp.model.database.MovieDao
import org.themoviedb.testapp.model.MovieDetailsResult
import org.themoviedb.testapp.model.MoviesListResult
import org.themoviedb.testapp.network.MoviesApi
import org.themoviedb.testapp.network.State
import retrofit2.Response
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieListRepository @Inject constructor(
    private var moviesApi: MoviesApi,
    private var movieDao: MovieDao,
    private val compositeDisposable: CompositeDisposable

) : PageKeyedDataSource<Int, Movie>() {

    private var filterString: String? = null
    private var state: MutableLiveData<State> = MutableLiveData()

    fun setFilterString(value: String) {
        this.filterString = value
    }

    fun getState(): MutableLiveData<State> {
        return state
    }

    fun getMovieApi(): MoviesApi {
        return moviesApi
    }

    fun getMovieDao(): MovieDao {
        return movieDao
    }


    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Movie>
    ) {
        //in this method shouldnt use asynch requests
        try {

            var result =
                if (movieDao.loadAll().size > 0) movieDao.loadAll() else moviesApi.getMoviesSync("1").execute().body()!!.results

            if (filterString!!.length > 0) {
                result = result.filter {
                    it.release_date.substring(0, 4).contentEquals(filterString!!)
                }
            }


            updateState(State.DONE)

            if (result.size > 0) {
                movieDao.insertAll(result)
                callback.onResult(result, null, 2)
            } else {
                updateState(State.ERROR)

            }
        } catch (e: Exception) {
            updateState(State.ERROR)

        }


    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {

        compositeDisposable.add(
            getMovieList(params.key).timeout(10, TimeUnit.SECONDS)
                .subscribe(
                    { response ->
                        if (response.isSuccessful) {
                            updateState(State.DONE)

                            callback.onResult(
                                if (filterString == null || filterString!!.length == 0) response.body()!!.results else response.body()!!.results.filter {
                                    if (it.release_date!=null )it.release_date.substring(0, 4).contentEquals(filterString!!)else true
                                },
                                params.key + 1
                            )
                        }
                        else {
                            updateState(State.ERROR)
                        }
                    },
                    {
                        updateState(State.ERROR)

                    }
                )
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {
    }

    private fun updateState(state: State) {
        this.state.postValue(state)
    }

    lateinit var data: Single<List<Movie>>


    fun getData(filterString: String = ""): Observable<List<Movie>> {


        return Observable.fromCallable { movieDao.getAllObservable() }.map { it.blockingFirst() }
            .concatMap { dbMovieList ->
                Observable.just(dbMovieList).map {
                    it.filter { movie ->
                        movie.release_date.substring(5, 7).contentEquals(
                            filterString
                        )
                    }


                }
            }
    }

    fun getMovieList(page: Int): Single<Response<MoviesListResult>> {

        return moviesApi.getMovies(page.toString())

    }

    fun getMovieDetails(movieId: Int): Single<MovieDetailsResult> {
        return moviesApi.getMovieDetails(movieId.toString())
    }


}