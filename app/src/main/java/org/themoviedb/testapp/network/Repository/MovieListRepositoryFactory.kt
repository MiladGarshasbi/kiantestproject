package org.themoviedb.testapp.network.Repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import io.reactivex.disposables.CompositeDisposable
import org.themoviedb.testapp.model.database.Movie
import org.themoviedb.testapp.model.database.MovieDao
import org.themoviedb.testapp.network.MoviesApi

class MovieListRepositoryFactory(private val moviesApi: MoviesApi, private val movieDao: MovieDao
                                 ,private val compositeDisposable: CompositeDisposable,private val filterString:String="") :
    DataSource.Factory<Int, Movie> (){
    val movieListRepositoryLiveData = MutableLiveData<MovieListRepository>()

    override fun create(): DataSource<Int, Movie> {
        val movieListRepository=MovieListRepository(moviesApi,movieDao,compositeDisposable)
        movieListRepository.setFilterString(filterString)
        movieListRepositoryLiveData.postValue(movieListRepository)
        return movieListRepository

    }
}