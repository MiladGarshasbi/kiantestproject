package org.themoviedb.testapp.network

enum class State {
    DONE, LOADING, ERROR
}