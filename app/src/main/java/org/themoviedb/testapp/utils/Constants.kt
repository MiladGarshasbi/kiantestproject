package org.themoviedb.testapp.utils

const val BASE_URL: String = "https://api.themoviedb.org/3/movie/"
const val IMAGE_BASE_URL:String="https://image.tmdb.org/t/p/w300/"
const val API_KEY: String = "d682f5115db118de7a8c9c279062ea12"
const val MOVIE_LIST_URL:String="popular?api_key=$API_KEY"
