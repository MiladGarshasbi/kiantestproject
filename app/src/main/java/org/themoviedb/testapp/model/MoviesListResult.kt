package org.themoviedb.testapp.model

import org.themoviedb.testapp.model.database.Movie

data class MoviesListResult(
    val page: Int,
    val results: List<Movie>,
    val total_pages: Int,
    val total_results: Int
)