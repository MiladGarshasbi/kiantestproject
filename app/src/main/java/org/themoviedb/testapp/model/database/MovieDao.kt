package org.themoviedb.testapp.model.database

import androidx.room.*
import io.reactivex.Observable
import org.themoviedb.testapp.model.database.Movie

@Dao
interface MovieDao {

    @Query("SELECT * From movie")
    fun loadAll():List<Movie>

    @Query("SELECT * From movie")
    fun getAllObservable():Observable<List<Movie>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(  movies:List<Movie> )

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: Movie)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun update(movie: Movie)


    @Query("DELETE  FROM movie")
    fun deleteAll()

    @Delete
    fun delet(movie: Movie)
}
