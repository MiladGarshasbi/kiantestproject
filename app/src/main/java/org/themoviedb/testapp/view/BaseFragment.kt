package org.themoviedb.testapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import org.themoviedb.testapp.injection.component.AppComponent
import org.themoviedb.testapp.injection.component.DaggerAppComponent
import org.themoviedb.testapp.injection.module.AppModule
import org.themoviedb.testapp.injection.module.RepositoryModul

abstract class BaseFragment : Fragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val injector: AppComponent = DaggerAppComponent.builder()
            .appModule(AppModule(activity!!.application))
            .repositoryModul(RepositoryModul(activity!!.application))
            .build()
        inject(injector)


    }

    private fun inject(injector: AppComponent) {
        when (this) {
            is MoviesListFragment -> injector.inject(this)
            is MoviewDetailsFragment -> injector.inject(this)
        }
    }


}


