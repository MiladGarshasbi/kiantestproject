package org.themoviedb.testapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.movies_list_fragment.*
import org.themoviedb.testapp.R
import org.themoviedb.testapp.databinding.MoviesListFragmentBinding
import org.themoviedb.testapp.model.database.Movie
import org.themoviedb.testapp.network.Repository.MovieListRepository
import org.themoviedb.testapp.network.State
import org.themoviedb.testapp.view.adapter.MoviesListAdapter
import org.themoviedb.testapp.viewmodel.MovieListViewModelFactory
import org.themoviedb.testapp.viewmodel.MoviesListViewModel
import javax.inject.Inject

class MoviesListFragment : BaseFragment(), MoviesListAdapter.OnMovieItemclickListener {

    private lateinit var binding: MoviesListFragmentBinding
    private lateinit var adapter: MoviesListAdapter
    private lateinit var viewModel: MoviesListViewModel


    @Inject
    lateinit var movieListRepository: MovieListRepository


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(layoutInflater, R.layout.movies_list_fragment, container, false)

        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this,
            MovieListViewModelFactory(movieListRepository)
        )
            .get(MoviesListViewModel::class.java)

        initAdapter()
        initState()
        initSpinner()
        binding.lifecycleOwner = this
        binding.viewModel = viewModel


    }

    private fun initSpinner() {
        var tiemsLoaded = 1
        val list_of_items = resources.getStringArray(R.array.years)
        val array_adapter =
            this.context?.let {
                ArrayAdapter(
                    it,
                    android.R.layout.simple_spinner_item,
                    list_of_items
                )
            }
        array_adapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = array_adapter
        spinner.setSelection(0, false)


        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                tiemsLoaded++
                if (tiemsLoaded == 1) {
                    return
                } else {
                    if (position != 0) {
                        viewModel.onFilterClick(
                            this@MoviesListFragment,
                            if (position == 1) "" else list_of_items[position]
                        )
                        startObserving()
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }


    override fun onItemClik(item: Movie, view: View) {
        val bundle = Bundle()
        bundle.putInt("id", item.id)
        view.findNavController()
            .navigate(R.id.action_moviesListFragment_to_moviewDetailsFragment, bundle)
    }


    private fun initAdapter() {
        adapter = MoviesListAdapter(this)
        movies_recyclerView.layoutManager =
            LinearLayoutManager(this.context, RecyclerView.VERTICAL, false)
        movies_recyclerView.adapter = adapter
        startObserving()


    }

    private fun startObserving() {
        viewModel.getMoviePagedList().observe(this, Observer {
            adapter.submitList(it)
            if (it.size == 0)
                Toast.makeText(
                    activity!!.applicationContext,
                    "No Item Found",
                    Toast.LENGTH_LONG
                ).show()
        })
    }

    private fun initState() {
        viewModel.getState().observe(this, Observer { state ->
            loading_progressBar.visibility =
                if (viewModel.listIsEmpty() && state == State.LOADING) View.VISIBLE else View.GONE

            if (state == State.ERROR)
                Toast.makeText(activity, "Netwok Connection Error", Toast.LENGTH_SHORT).show()
            if (!viewModel.listIsEmpty()) {
                adapter.setState(state ?: State.DONE)
            }
        })
    }


}
