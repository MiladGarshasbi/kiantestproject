package org.themoviedb.testapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.movies_details_fragment.*
import org.themoviedb.testapp.R
import org.themoviedb.testapp.databinding.MoviesDetailsFragmentBinding
import org.themoviedb.testapp.model.MovieDetailsResult
import org.themoviedb.testapp.network.Repository.MovieListRepository
import org.themoviedb.testapp.network.State
import org.themoviedb.testapp.utils.IMAGE_BASE_URL
import org.themoviedb.testapp.utils.setImageUrl
import org.themoviedb.testapp.viewmodel.MovieDetailsViewModel
import org.themoviedb.testapp.viewmodel.MoviesDetailsViewModelFactory
import javax.inject.Inject

class MoviewDetailsFragment : BaseFragment() {

    private lateinit var binding: MoviesDetailsFragmentBinding
    private lateinit var viewModel: MovieDetailsViewModel


    @Inject
    lateinit var movieListRepository: MovieListRepository

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.movies_details_fragment,
                container,
                false
            )

        binding.lifecycleOwner = this


        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProviders.of(
                this,
                MoviesDetailsViewModelFactory(
                    arguments!!.getInt("id"),
                    movieListRepository
                )
            )
                .get(MovieDetailsViewModel::class.java)

        viewModel.getResult().observe(viewLifecycleOwner, Observer {
            updateUi(it)
        })

        viewModel.getState().observe(viewLifecycleOwner, Observer {
            if (it.equals(State.ERROR))
                Toast.makeText(activity, "Netwok Connection Error", Toast.LENGTH_SHORT).show()
        })
        binding.viewModel = viewModel

    }

    private fun updateUi(it: MovieDetailsResult) {

        cover_imageView.setImageUrl(IMAGE_BASE_URL + it.poster_path)
    }

}
