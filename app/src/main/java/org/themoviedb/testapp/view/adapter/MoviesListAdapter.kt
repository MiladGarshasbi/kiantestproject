package org.themoviedb.testapp.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_movie.view.*
import org.themoviedb.testapp.R
import org.themoviedb.testapp.databinding.ItemMovieBinding
import org.themoviedb.testapp.model.database.Movie
import org.themoviedb.testapp.network.State
import org.themoviedb.testapp.viewmodel.MovieViewModel
import java.lang.Exception

class MoviesListAdapter(private var onMovieItemclickListener: OnMovieItemclickListener) : PagedListAdapter<Movie,MoviesListAdapter.ViewHolder>(
    MoviesDiffCallback) {

    private var state = State.LOADING

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemMovieBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_movie, parent, false
        )

        return ViewHolder(binding)

    }

    override fun getItemCount(): Int {
           return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(getItem(position)!!)
            holder.itemView.imageView.setOnClickListener {
                onMovieItemclickListener.onItemClik(this.getItem(position)!!,it)
            }
        }catch (e :Exception){}



    }


    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }


    class ViewHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private var viewModel = MovieViewModel()


        fun bind(movie: Movie) {
            viewModel.bind(movie)
            binding.viewModel = viewModel
        }


    }


    interface OnMovieItemclickListener{
        fun onItemClik(item: Movie, view: View)
    }


    companion object {
        val MoviesDiffCallback = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem == newItem
            }
        }
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }




}

