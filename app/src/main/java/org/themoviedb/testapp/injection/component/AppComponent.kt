package org.themoviedb.testapp.injection.component

import android.app.Application
import dagger.Component
import org.themoviedb.testapp.injection.module.AppModule
import org.themoviedb.testapp.injection.module.RepositoryModul
import org.themoviedb.testapp.model.database.AppDatabase
import org.themoviedb.testapp.model.database.MovieDao
import org.themoviedb.testapp.network.MoviesApi
import org.themoviedb.testapp.network.Repository.MovieListRepository
import org.themoviedb.testapp.view.MoviesListFragment
import org.themoviedb.testapp.view.MoviewDetailsFragment
import org.themoviedb.testapp.viewmodel.MovieDetailsViewModel
import org.themoviedb.testapp.viewmodel.MoviesListViewModel
import javax.inject.Singleton


@Singleton
@Component(modules = [(AppModule::class), (RepositoryModul::class)])
interface AppComponent {

    fun inject(fragment: MoviesListFragment)
    fun inject(fragment: MoviewDetailsFragment)

    fun inject(viewModel: MoviesListViewModel)

    fun inject(viewModel: MovieDetailsViewModel)

    fun movieDao(): MovieDao

    fun movieApi():MoviesApi

    fun appDatabase(): AppDatabase

    fun movieListRepository():MovieListRepository

    fun application():Application





}