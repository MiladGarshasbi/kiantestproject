package org.themoviedb.testapp.injection.module

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.themoviedb.testapp.model.database.AppDatabase
import org.themoviedb.testapp.model.database.MovieDao
import org.themoviedb.testapp.network.MoviesApi
import org.themoviedb.testapp.network.Repository.MovieListRepository
import org.themoviedb.testapp.network.getUnsafeOkHttpClient
import org.themoviedb.testapp.utils.BASE_URL
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class RepositoryModul(mApplication: Application) {

    private var database: AppDatabase

    init {
        database =
            Room.databaseBuilder(mApplication.applicationContext, AppDatabase::class.java, "movies").build()
    }

    @Singleton
    @Provides
    fun proviedAppDataBase(): AppDatabase {
        return database
    }

    @Singleton
    @Provides
    internal fun provideDBMovieDao(): MovieDao {
        return (database.movieDao())
    }




    @Provides
    @Singleton
    internal fun provideMoviesApi(retrofit: Retrofit): MoviesApi {
        return retrofit.create(MoviesApi::class.java)
    }



    @Singleton
    @Provides
    internal fun provideRetrofitInterface(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(getUnsafeOkHttpClient().build())
            .build()

    }

    @Provides
    internal fun providesCompositDisposable():CompositeDisposable{
        return CompositeDisposable()
    }

    @Provides
    @Singleton
    internal fun provideMovieListReposirory(moviesApi: MoviesApi, movieDao: MovieDao, compositeDisposable: CompositeDisposable): MovieListRepository {
        return MovieListRepository(moviesApi,movieDao,compositeDisposable)
    }



}


