package org.themoviedb.testapp

import androidx.room.Room
import androidx.test.filters.SmallTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.themoviedb.testapp.model.database.AppDatabase
import org.themoviedb.testapp.model.database.Movie
import org.themoviedb.testapp.model.database.MovieDao
import java.io.IOException


@RunWith(AndroidJUnit4::class)
@SmallTest
class DbDaoTest {
    private lateinit var db: AppDatabase
    private lateinit var movieDao: MovieDao

    @Before
    fun createDataBase() {
        val context = InstrumentationRegistry.getInstrumentation().context
        db = Room.inMemoryDatabaseBuilder(
            context,
            AppDatabase::class.java
        ).build()
        movieDao = db.movieDao()
    }

    @After
    @Throws(IOException::class)
    fun cloaseDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun movieDao() {
        assertEquals(movieDao.loadAll().size == 0, true)
        val movie = getMovie(0)
        movieDao.insert(movie)
        assertEquals(movieDao.loadAll()[0].original_title, "lion king")
        assertEquals(movieDao.loadAll().size, 1)
        movieDao.deleteAll()
        assertEquals(movieDao.loadAll().size, 0)
        movieDao.insertAll(getListOfMovie())
        assertEquals(movieDao.loadAll().size,20)
        movieDao.delet(getMovie(1))
        assertEquals(movieDao.loadAll().size,19)

    }

    private fun getMovie(id:Int): Movie {
        val movie = Movie(
            id, true, "", "", "lion king",
            "", 234.3, "", "", "", true, 23.32, 23
        )
        return movie
    }

    private fun getListOfMovie(): List<Movie> {
        val list = arrayListOf<Movie>()
        for (item in 1..20) {
            val movie = getMovie(item)
            list.add(movie)

        }
        return list
    }
}